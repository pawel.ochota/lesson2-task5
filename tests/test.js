const isMaleName = require('..');

describe('isMaleName', () => {
  it('should returns a boolean', async () => {
    const result = isMaleName('test');

    expect(typeof result).toBe('boolean');
  });

  it('should returns false when last letter for name is "a"', async () => {
    expect(isMaleName('Aga')).toBe(false);
    expect(isMaleName('AGA')).toBe(false);
    expect(isMaleName('KAtARZYNA')).toBe(false);
    expect(isMaleName('Daria')).toBe(false);
  });

  it('should returns true when last letter for name isn\'t "a"', async () => {
    expect(isMaleName('Tomek')).toBe(true);
    expect(isMaleName('Bartosz')).toBe(true);
    expect(isMaleName('JAN')).toBe(true);
    expect(isMaleName('Zbysiu')).toBe(true);
  });

  it('should returns true when paremeter is "Bonawentura"', async () => {
    expect(isMaleName('Bonawentura')).toBe(true);
    expect(isMaleName('BONAWENTURA')).toBe(true);
    expect(isMaleName('bonawentura')).toBe(true);
  });
});
